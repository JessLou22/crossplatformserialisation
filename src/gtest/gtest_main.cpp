
#include <cstdlib>


#include <limits.h>

#include "gtest/gtest.h"

#ifndef _WIN32
const char** __argv;
int __argc;
#endif



int main(int argc, char **argv)
{
    ::testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}
