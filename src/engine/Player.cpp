#include "Player.h"
#include "Math.h"

#include "InputMemoryBitStream.h"
#include "OutputMemoryBitStream.h"

//zoom hardcoded at 100...if we want to lock players on screen, this could be calculated from zoom
const float HALF_WORLD_HEIGHT = 3.6f;
const float HALF_WORLD_WIDTH = 6.4f;

Player::Player() :
	GameObject(),
	mVelocity( Vector3::Zero ),
	mMaxLinearSpeed( 50.f ),
	mMaxRotationSpeed( 5.f ),
	mWallRestitution( 0.1f ),
	mNPCRestitution( 0.1f ),
	mPlayerId( 0 ),
	mLastMoveTimestamp ( 0.0f ),
	mThrustDir( 0.f ),
	mHealth( 10 ),
	mIsShooting( false )

{
	SetCollisionRadius( 0.5f );
}

void Player::ProcessInput( float inDeltaTime, const InputState& inInputState )
{
	//process our input....

	//turning...

	//moving...

	//shooting

}

void Player::AdjustVelocityByThrust( float inDeltaTime )
{
	//just set the velocity based on the thrust direction -- no thrust will lead to 0 velocity
	//simulating acceleration makes the client prediction a bit more complex
	Vector3 forwardVector = GetForwardVector();
	mVelocity = forwardVector * ( mThrustDir * inDeltaTime * mMaxLinearSpeed );
}

void Player::SimulateMovement( float inDeltaTime )
{
	//simulate us...
	AdjustVelocityByThrust( inDeltaTime );

	SetLocation( GetLocation() + mVelocity * inDeltaTime );

	ProcessCollisions();
}

void Player::Update()
{

}

void Player::ProcessCollisions()
{
	//right now just bounce off the sides..
	ProcessCollisionsWithScreenWalls();

	float sourceRadius = GetCollisionRadius();
	Vector3 sourceLocation = GetLocation();

	//now let's iterate through the world and see what we hit...
	//this requires a world - probably a singleton. We don't have one, so forget
	//it for now.

	//note: if there's a small number of objects in our game, iterating though is fine.
	//but in a real game, brute-force checking collisions against every other object is not efficient.
	//it would be preferable to use a quad tree or some other structure to minimize the
	//number of collisions that need to be tested.

}

void Player::ProcessCollisionsWithScreenWalls()
{
	Vector3 location = GetLocation();
	float x = location.mX;
	float y = location.mY;

	float vx = mVelocity.mX;
	float vy = mVelocity.mY;

	float radius = GetCollisionRadius();

	//if the cat collides against a wall, the quick solution is to push it off
	if( ( y + radius ) >= HALF_WORLD_HEIGHT && vy > 0 )
	{
		mVelocity.mY = -vy * mWallRestitution;
		location.mY = HALF_WORLD_HEIGHT - radius;
		SetLocation( location );
	}
	else if( y <= ( -HALF_WORLD_HEIGHT - radius ) && vy < 0 )
	{
		mVelocity.mY = -vy * mWallRestitution;
		location.mY = -HALF_WORLD_HEIGHT - radius;
		SetLocation( location );
	}

	if( ( x + radius ) >= HALF_WORLD_WIDTH && vx > 0 )
	{
		mVelocity.mX = -vx * mWallRestitution;
		location.mX = HALF_WORLD_WIDTH - radius;
		SetLocation( location );
	}
	else if(  x <= ( -HALF_WORLD_WIDTH - radius ) && vx < 0 )
	{
		mVelocity.mX = -vx * mWallRestitution;
		location.mX = -HALF_WORLD_WIDTH - radius;
		SetLocation( location );
	}
}

uint32_t Player::Write( OutputMemoryBitStream& inOutputStream, uint32_t inDirtyState ) const
{
	uint32_t writtenState = 0;

	if( inDirtyState & ECRS_PlayerId )
	{
		inOutputStream.Write( (bool)true );
		inOutputStream.Write( GetPlayerId() );

		writtenState |= ECRS_PlayerId;
	}
	else
	{
		inOutputStream.Write( (bool)false );
	}


	if( inDirtyState & ECRS_Pose )
	{
		inOutputStream.Write( (bool)true );

		Vector3 velocity = mVelocity;
		inOutputStream.Write( velocity.mX );
		inOutputStream.Write( velocity.mY );

		Vector3 location = GetLocation();
		inOutputStream.Write( location.mX );
		inOutputStream.Write( location.mY );

		inOutputStream.Write( GetRotation() );

		writtenState |= ECRS_Pose;
	}
	else
	{
		inOutputStream.Write( (bool)false );
	}

	//always write mThrustDir- it's just two bits
	if( mThrustDir != 0.f )
	{
		inOutputStream.Write( true );
		inOutputStream.Write( mThrustDir > 0.f );
	}
	else
	{
		inOutputStream.Write( false );
	}

	if( inDirtyState & ECRS_Color )
	{
		inOutputStream.Write( (bool)true );
		inOutputStream.Write( GetColor() );

		writtenState |= ECRS_Color;
	}
	else
	{
		inOutputStream.Write( (bool)false );
	}

	if( inDirtyState & ECRS_Health )
	{
		inOutputStream.Write( (bool)true );
		inOutputStream.Write( mHealth, 4 );

		writtenState |= ECRS_Health;
	}
	else
	{
		inOutputStream.Write( (bool)false );
	}

	return writtenState;

}

void Player::Read( InputMemoryBitStream& inInputStream )
{
	bool stateBit;

	uint32_t readState = 0;

	inInputStream.Read( stateBit );
	if( stateBit )
	{
		uint32_t playerId;
		inInputStream.Read( playerId );
		SetPlayerId( playerId );
		readState |= ECRS_PlayerId;
	}

	float oldRotation = GetRotation();
	Vector3 oldLocation = GetLocation();
	Vector3 oldVelocity = GetVelocity();

	float replicatedRotation;
	Vector3 replicatedLocation;
	Vector3 replicatedVelocity;

	inInputStream.Read( stateBit );
	if( stateBit )
	{
		inInputStream.Read( replicatedVelocity.mX );
		inInputStream.Read( replicatedVelocity.mY );

		SetVelocity( replicatedVelocity );

		inInputStream.Read( replicatedLocation.mX );
		inInputStream.Read( replicatedLocation.mY );

		SetLocation( replicatedLocation );

		inInputStream.Read( replicatedRotation );
		SetRotation( replicatedRotation );

		readState |= ECRS_Pose;
	}

	inInputStream.Read( stateBit );
	if( stateBit )
	{
		inInputStream.Read( stateBit );
		mThrustDir = stateBit ? 1.f : -1.f;
	}
	else
	{
		mThrustDir = 0.f;
	}

	inInputStream.Read( stateBit );
	if( stateBit )
	{
		Vector3 color;
		inInputStream.Read( color );
		SetColor( color );
		readState |= ECRS_Color;
	}

	inInputStream.Read( stateBit );
	if( stateBit )
	{
		mHealth = 0;
		inInputStream.Read( mHealth, 4 );
		readState |= ECRS_Health;
	}

	// Game Logic?
	// Need to inform the game of  changes to player
	// E.g. Update the HUD with health etc.
	// Also handle re-joining messages i.e. what if the server is
	// using this to read and gets a 'hello' message. 

}

bool Player::operator==(Player &other)
{
	// Game Object Part.
	//Call the == of the base, Player reference is
	//downcast explicitly.
	if(!GameObject::operator==(other)) return false;

	if(this->ECRS_AllState != other.ECRS_AllState) return false;

  if (!Math::Is3DVectorEqual(this->mVelocity, other.mVelocity)) return false;
	if (!Math::FP_EQUAL(this->mMaxLinearSpeed, other.mMaxLinearSpeed)) return false;
	if (!Math::FP_EQUAL(this->mMaxRotationSpeed, other.mMaxRotationSpeed)) return false;
  if (!Math::FP_EQUAL(this->mWallRestitution, other.mWallRestitution)) return false;
	if (!Math::FP_EQUAL(this->mNPCRestitution, other.mNPCRestitution)) return false;
	if(this->mPlayerId != other.mPlayerId) return false;

  if (!Math::FP_EQUAL(this->mLastMoveTimestamp, other.mLastMoveTimestamp)) return false;
	if (!Math::FP_EQUAL(this->mThrustDir, other.mThrustDir)) return false;
	if(this->mHealth != other.mHealth) return false;
	if(this->mIsShooting != other.mIsShooting) return false;

	return true;
}
