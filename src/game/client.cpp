#include "LinkingContext.h"
#include "Player.h"

#include "InputMemoryBitStream.h"
#include "OutputMemoryBitStream.h"

#include <cstring>
#include <iostream>

void test();
void fakeNetworkActivity(const char* up, int upSize, char* down);

#if _WIN32
int WINAPI WinMain( _In_ HINSTANCE hInstance, _In_opt_ HINSTANCE hPrevInstance, _In_ LPSTR lpCmdLine, _In_ int nCmdShow )
{
	UNREFERENCED_PARAMETER( hPrevInstance );
	UNREFERENCED_PARAMETER( lpCmdLine );

  test();
}
#else
const char** __argv;
int __argc;
int main(int argc, const char** argv)
{
	__argc = argc;
	__argv = argv;

  test();
}
#endif

void test()
{
  shared_ptr<LinkingContext>world(new LinkingContext());
  PlayerPtr player(static_cast<Player*>(Player::StaticCreate()));
  uint32_t state; //flags - what needs to be sent of player.
  shared_ptr<OutputMemoryBitStream> output(new OutputMemoryBitStream());
  shared_ptr<InputMemoryBitStream> input;

  GameObject* playerGO = static_cast<GameObject*>(player.get());
  player->SetNetworkId(world->GetNetworkId(playerGO, true));
  player->SetPlayerId(1);

  std::cout << "Original" << std::endl  << "\tPlayer id: " << player->GetPlayerId() << std::endl << "\tNetwork Id: " << player->GetNetworkId() << std::endl;


  //state = player->GetAllStateMask();
  state = 0x000F; // Set all bits as dirty.

  //write it into a buffer.
  player->Write(*output,state);

  /* We've looked at sending bytes between machines, assume we did
  that here and the then received a response */

  char* incomming = new char[256];
  fakeNetworkActivity(output->GetBufferPtr(), output->GetByteLength(), incomming);

  std::cout << "Lenght of encoded player (all fields dirty): " << output->GetByteLength() << std::endl;

  // Change player, these should get overwritten.
  player->SetNetworkId(0);
  player->SetPlayerId(0);

  /* end of fakery ... back to work */

  input.reset(new InputMemoryBitStream(incomming, 256));

  // update from our 'server'.
  player->Read(*input);

  // server is just echoing so they should be the same.
  std::cout << "Recovered" << std::endl  << "\tPlayer id: " << player->GetPlayerId() << std::endl << "\tNetwork Id: " << player->GetNetworkId() << std::endl;


  int netId = world->GetNetworkId(playerGO, true);
  player->SetNetworkId(netId);

  std::cout << "Player NetID as reported by world: " << netId << std::endl;
  std::cout << "Should be as above, indicating that the player already exists in this world" << std::endl;

  //state = Player::ECRS_Pose;
  state = 0x0001; // just pose info

  //create a new bitstream (no method to reset)
  output.reset(new OutputMemoryBitStream());

  //write it into a buffer.
  player->Write(*output,state);

  std::cout << "Lenght of encoded player (just pose info): " << output->GetByteLength() << std::endl;

}


void fakeNetworkActivity(const char* up, int upSize, char* down)
{
    // ... imagine networking goes on and we get an
  // actually we're connecting the output buffer to the input.
  // copy the buffer first (or we get double de-allocation)

  memcpy(down, up, upSize);
}
